//ヘッダー
#include"im.h"

/*
 *最終課題用のプログラム
 *目標はべた塗りの描画を再現できる画像フィルタの作成
 *
 *期限は7/29(火)！！！
 *
 */

//プロトタイプ関数の宣言
void printHelp( char *);						//ヘルプメッセージを出力するための関数
GLvoid initgfx( GLvoid );						//初期化処理を行う関数
GLvoid drawScene( GLvoid );					//描画処理用の関数
GLvoid reshape( GLsizei , GLsizei );			//画面サイズを変更するための関数
GLvoid keyboard( GLubyte , GLint , GLint );		//キーボード処理を行うための関数

// キーボードASCII値
#define KEY_ESC	27

//ウィンドウのサイズ
static int winWidth;							//ウィンドウの横幅
static int winHeight;						//ウィンドウの高さ

//テクスチャデータ（後で画像データを入れるので最初はNULL）
static ms2dImage *imgSrc = NULL;
static ms2dImage *imgDst = NULL;


//　メイン関数
void main( int argc , char *argv[] ){

	//　GLUTにmainの引数を渡して初期化する
		glutInit( &argc , argv );

	/*　ウィンドウの設定　*/

	//　GLUTのスクリーンサイズの取得を行う
		GLsizei width  = glutGet( GLUT_SCREEN_WIDTH );
		GLsizei height = glutGet( GLUT_SCREEN_HEIGHT );

	//　ウィンドウの諸設定
		glutInitWindowPosition( width / 8 , height / 8);
		glutInitWindowSize( width * 6 / 8 , height * 6 / 8 );

	//　色の設定
		glutInitDisplayMode( GLUT_RGBA | GL_DOUBLE );

	//　ウィンドウの作成（argv[0]はタイトル）
		glutCreateWindow( argv[0] );

	//　ヘルプメッセージ
		printHelp( argv[1] );

	/*　画像処理　*/

	//　元画像の読み込み
		imgSrc = ms2dImageCreateWithFile("hall.bmp");

	//　結果画像を用意
		imgDst = ms2dImageCreate( 1 , 1 );

	//　初期化処理
		initgfx();

	//　再描画関数の定義
		glutKeyboardFunc( keyboard );
		glutReshapeFunc( reshape );
		glutDisplayFunc( drawScene );

	/*　その他　*/

	//　メインループ（GLUTに委譲）
		glutMainLoop();

}



//　ヘルプメッセージ
void printHelp( char *progname ){

	//　ここにコンソールで出力したい内容を書く
		fprintf( stdout , //以下記述欄
			//　ここから
			"M0112066　内田 敏樹　最終課題\n"
			"課題内容「べた塗り風画像フィルター」\n\n"
			"[L] -> 画像の読み込みを行います\n"
			"[S] -> 結果画像の保存を行います\n"
			"[a] -> 線画風の画像を出力します\n"
			"[m] -> メディアンフィルタを実行し、結果を出力します\n"
			"[M] -> 5×5の画素を対象にラプラシアンを実行し、結果を出力します\n"
			"[l] -> ラプラシアンフィルタを実行し、結果を出力します\n"
			"[k] -> ラプラシアンとメディアンを同時に実行し、合わせた結果を出力します\n"
			"[g] -> HSVで明度をとりグレースケールで10段階の明るさを出力する\n" 
			"[G] -> 漫画風の画像を出力します\n"
			"[w] -> 明度を基にグレースケールフィルタを実行し、結果を出力します\n"
			"[W] -> RGB値を基にグレースケールフィルタを実行し、結果を出力します\n"
			"ESC -> プログラムを終了します\n\n"
			//　ここまで
			, progname );

}



//　初期化処理
GLvoid initgfx( GLvoid ){

	//　背景の色
		glClearColor( 0.85 , 0.88 , 0.9 , 1.0 );

}



//　画面サイズを変更
GLvoid reshape( GLsizei width , GLsizei height ){

	//　ウィンドウ枠のビューポートを設定
		glViewport( 0 , 0 , width , height );

	//　ウィンドウサイズの更新
		winWidth = width;
		winHeight = height;

}



//　キーボード処理
GLvoid keyboard( GLubyte key , GLint x , GLint y ){

	//　それぞれのキーボードを押した時の処理
		switch( key ){

		//”L”を入力
			case 'L':
			{
			
			//　読み込み用のファイルダイアログを開く
				char path[256];
				if(msglOpenDialog(path)){

				//　imgSrcにファイルを読み込む
					ms2dImageLoadFile( imgSrc , path );

				//　msglのテクスチャを新しいデータに差し替え
					msglReplaceTexture(imgSrc);

				//　再描画
					glutPostRedisplay();

				}
				break;
			}
			case 'S':
			{
				//　保蔵用ファイルダイアログを開く
					char path[256];
					if( msglSaveDialog(path)){
					//　imgDisをファイルに保存する
						ms2dImageSaveFile( imgDst , path );
					}
			}
			case 'a':
			{
				//　べた塗りフィルタ
					imFilterLineDrawing( imgSrc , imgDst );
				//　msglのテクスチャを新しいデータに差し替え
					msglReplaceTexture( imgDst );
				//　再描画
					glutPostRedisplay();
					break;
			}
			case 'm':
			{
				//　メディアンフィルタ
					imFilterMed( imgSrc , imgDst );
				//　msglのテクスチャを新しいデータに差し替え
					msglReplaceTexture( imgDst );
				//　再描画
					glutPostRedisplay();
					break;
			}
			case 'M':
			{
				//　メディアンフィルタ
					imFilterNorLap( imgSrc , imgDst );
				//　msglのテクスチャを新しいデータに差し替え
					msglReplaceTexture( imgDst );
				//　再描画
					glutPostRedisplay();
					break;
			}
			case 'l':
			{
				//　メディアンとラプラシアン
					imFilterLap( imgSrc , imgDst );
				//　msglのテクスチャを新しいデータに差し替え
					msglReplaceTexture( imgDst );
				//　再描画
					glutPostRedisplay();
					break;
			}
			case 'k':
			{
				//　メディアンとラプラシアン
					imFilterMedLap( imgSrc , imgDst );
				//　msglのテクスチャを新しいデータに差し替え
					msglReplaceTexture( imgDst );
				//　再描画
					glutPostRedisplay();
					break;
			}

			case 'g':
			{
				//　グレースケールで色を絞って出力
					imFileter10Scale( imgSrc , imgDst );
				//　msglのテクスチャを新しいデータに差し替え
					msglReplaceTexture( imgDst );
				//　再描画
					glutPostRedisplay();
					break;
			}
			case 'G':
			{
				//　グレースケールで色を絞って出力
					imFileter5ScaleComic( imgSrc , imgDst );
				//　msglのテクスチャを新しいデータに差し替え
					msglReplaceTexture( imgDst );
				//　再描画
					glutPostRedisplay();
					break;
			}
			case 'c':
			{
				//　グレースケールで色を絞って出力
					imFileterBrightMed( imgSrc , imgDst );
				//　msglのテクスチャを新しいデータに差し替え
					msglReplaceTexture( imgDst );
				//　再描画
					glutPostRedisplay();
					break;
			}
			case 'C':
			{
				//　グレースケールで色を絞って出力
					imFileter5ScaleColor( imgSrc , imgDst );
				//　msglのテクスチャを新しいデータに差し替え
					msglReplaceTexture( imgDst );
				//　再描画
					glutPostRedisplay();
					break;
			}
			case'w':
			{
				imFilterWhiteScale(imgSrc, imgDst);
				msglReplaceTexture(imgDst);
				glutPostRedisplay();
				break;
			}
			case'W':
			{
				imFilterGrayScale(imgSrc, imgDst);
				msglReplaceTexture(imgDst);
				glutPostRedisplay();
				break;
			}



		//　プログラムの終了処理
			case KEY_ESC:
			{
				exit(0);
			}
		}
}



//　描画処理
GLvoid drawScene( GLvoid ){

	//　初期化
		glClear( GL_COLOR_BUFFER_BIT );

	//　msglの行列をセットする
		msglPushDefaultMatrix( winWidth , winHeight );
		{
		//　元画像のmsglテクスチャ平面を表示
			msglTexQuad( imgSrc );

		//　結果画像のテクスチャ平面を表示
			glTranslatef( imgSrc -> width + 10 , 0 , 0 );
			msglTexQuad( imgDst );
		}

	//　msglの行列をクリアする
		msglPopMatrix();

	//　強制更新
		glutSwapBuffers();

}