//ヘッダー
#include"im.h"

//以下"im.h"で作った関数の内容
int edgeMultiplyLess(int rgb, int lap){
	return ( 2 * ( ( (double)rgb / 255.0 ) * ( (double)lap / 255.0 ) ) ) * 255;
}

int edgeMultiplyMore(int rgb, int lap){
	return ( 1 - ( 2 * ( 1 - ((double)rgb / 255.0) ) * ( 1 - ((double)lap / 255.0 ) ) ) ) * 255;
}

void clamp(int &r, int &g, int &b){
	if(r > 255) r = 255;
	else if(r < 0) r = 0;
	if(g > 255) g = 255;
	else if(g < 0) g = 0;
	if(b > 255) b = 255;
	else if(b < 0) b = 0;
}

int retrieve5thGreatest(int rgb, int k[9]){
	int filterNum = 0;
	for( int i = 0; i < 5 ; i++){
	rgb = 0;
	for(int j = 0; j < 9 ; j++){
						
		if( rgb <= k[j] ){
			rgb = k[j] ;
			filterNum = j ;
		}
	}
	k[filterNum] = -1;
	}
	return rgb;
}

int pixelsValueGrouping(int rgb, double v, int i){
	//　0.1ごとに条件を決めて色を定める
	if( v >= ( 0.5 * i ) && v < ( 0.5 * ( i + 1 ) ) ){
	//　半分の値をDstに加える
		rgb = (int)((0.5 * ( i + 2.5 )) * 255);
	}else if( v == 1.0 ){
		rgb = 255;
	}
	return rgb;
}

double maxColorValue(int r, int g, int b){
	double  maxv = max( r , g );
			maxv = max( maxv , b );
	return maxv;
}

double minColorValue(int r, int g, int b){
	double  minv = min(r, g);
			minv = min(minv, b);
	return minv;
}

//　メディアンとラプラシアンを同時に行う関数
void imFilterMedLap( ms2dImage *imgSrc , ms2dImage *imgDst ){
	ms2dImage *imgMed = ms2dImageCreate(1, 1);
	ms2dImage *imgLapAfterMed = ms2dImageCreate(1, 1);

	imFilterMed(imgSrc, imgMed);
	imFilterLap(imgMed, imgLapAfterMed);
	
	ms2dImageCopy(imgLapAfterMed, imgDst);
}

//　ラプラシアンフィルタ
void imFilterLap( ms2dImage *imgSrc , ms2dImage *imgDst ){

	//　Dstにコピー
		ms2dImageCopy( imgSrc , imgDst );

	//　画像サイズを取得
		const int wid = imgDst -> width;					//　幅
		const int hgt = imgDst -> height;					//　高さ

	//　ピクセルアクセスの準備
		ms2dColorMatrix pxSrc = imgSrc -> pixels;		//　元画像のピクセルデータ
		ms2dColorMatrix pxDst = imgDst -> pixels;		//　結果画像のピクセルデータ

	//　フィルタ行列

		/* 横方向の移動量 */
		const int dx[9] = {	-1	,	0	,	1	,
							-1	,	0	,	1	,
							-1	,	0	,	1	};

		/* 縦方向の移動量 */
		const int dy[9] = {	-1	,	-1	,	-1	,
							 0	,	 0	,	 0	,
							 1	,	 1	,	 1	};

		/* フィルタ係数 */
		const int k[9]  = {	1	,	1	,	1	,
							1	,	-8	,	1	,
							1	,	1	,	1	};

	// 1ピクセルずつ、すべてのピクセルを処理
	// 空間フィルタは周囲のピクセルを参照するため、画面の端は処理しない
		for(int y = 1; y < hgt-1; y++)
		{
			for(int x = 1; x < wid-1; x++)
			{
			// 合計値を入れる変数（最初は0に初期化)
				int RSum = 0;
				int GSum = 0;
				int BSum = 0;
			// 周辺9画素を計算に使う
				for(int i = 0; i < 9; i++)
				{
				// 元画像pxSrcの(x+dx[i], y+dy[i])座標の色値を得て、フィルタ係数をかける
					RSum += pxSrc[y+dy[i]][x+dx[i]].R * k[i];
					GSum += pxSrc[y+dy[i]][x+dx[i]].G * k[i];
					BSum += pxSrc[y+dy[i]][x+dx[i]].B * k[i];
				}
			// 結果画像に値を代入
				int RDst = RSum;
				int GDst = GSum;
				int BDst = BSum;
			//　色の補正
				clamp(RDst, GDst, BDst);
				int Dst = ( RDst + GDst + BDst ) / 3;
			// 結果画像pxDstの(x,y)の色値を更新
				pxDst[y][x].R = Dst;
				pxDst[y][x].G = Dst;
				pxDst[y][x].B = Dst;
			}
		}

}

//　ラプラシアンフィルタ
void imFilterNorLap( ms2dImage *imgSrc , ms2dImage *imgDst ){

	//　Dstにコピー
		ms2dImageCopy( imgSrc , imgDst );

	//　画像サイズを取得
		const int wid = imgDst -> width;					//　幅
		const int hgt = imgDst -> height;					//　高さ

	//　ピクセルアクセスの準備
		ms2dColorMatrix pxSrc = imgSrc -> pixels;		//　元画像のピクセルデータ
		ms2dColorMatrix pxDst = imgDst -> pixels;		//　結果画像のピクセルデータ

	//　フィルタ行列

		/* 横方向の移動量 */
		const int dx[25] = {	-2	,	-1	,	 0	,	 1	,	 2	,
								-2	,	-1	,	 0	,	 1	,	 2	,
								-2	,	-1	,	 0	,	 1	,	 2	,
								-2	,	-1	,	 0	,	 1	,	 2	,
								-2	,	-1	,	 0	,	 1	,	 2	};

		/* 縦方向の移動量 */
		const int dy[25] = {	-2	,	-2	,	-2	,	-2	,	-2	,
								-1	,	-1	,	-1	,	-1	,	-1	,
								 0	,	 0	,	 0	,	 0	,	 0	,
								 1	,	 1	,	 1	,	 1	,	 1	,
								 2	,	 2	,	 2	,	 2	,	 2	};

		/* フィルタ係数 */
		const int k[25] = {	-1	,	-3	,	-4	,	-3	,	-1	,
							-3	,	 0	,	 6	,	 0	,	-3	,
							-4	,	 6	,	20	,	 6	,	-4	,
							-3	,	 0	,	 6	,	 0	,	-3	,
							-1	,	-3	,	-4	,	-3	,	-1	};

	// 1ピクセルずつ、すべてのピクセルを処理
	// 空間フィルタは周囲のピクセルを参照するため、画面の端は処理しない
		for(int y = 2; y < hgt-2; y++)
		{
			for(int x = 2; x < wid-2; x++)
			{
			// 合計値を入れる変数（最初は0に初期化)
				int RSum = 0;
				int GSum = 0;
				int BSum = 0;
			// 周辺9画素を計算に使う
				for(int i = 0; i < 25; i++)
				{
				// 元画像pxSrcの(x+dx[i], y+dy[i])座標の色値を得て、フィルタ係数をかける
					RSum += pxSrc[y+dy[i]][x+dx[i]].R * k[i];
					GSum += pxSrc[y+dy[i]][x+dx[i]].G * k[i];
					BSum += pxSrc[y+dy[i]][x+dx[i]].B * k[i];
				}
			// 結果画像に値を代入
				int RDst = RSum ;
				int GDst = GSum ;
				int BDst = BSum ;
			//　色の補正
				clamp(RDst, GDst, BDst);
				int Dst = ( RDst + GDst + BDst ) / 3;
			// 結果画像pxDstの(x,y)の色値を更新
				pxDst[y][x].R = Dst;
				pxDst[y][x].G = Dst;
				pxDst[y][x].B = Dst;
			}
		}	
}

//　カラーメディアンフィルタ
void imFilterMed(ms2dImage *imgSrc, ms2dImage *imgDst)
{
	// Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	// 画像サイズを取得
	const int wid = imgDst->width;	// 幅
	const int hgt = imgDst->height;	// 高さ

	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ

	/*　横方向の移動量(3×3)　*/
	const int dx[9] =	{	-1	,	0	,	1	,
							-1	,	0	,	1	,
							-1	,	0	,	1	};
	/*　縦方向の移動量(3×3)　*/
	const int dy[9] =	{	-1	,	-1	,	-1	,
							 0	,	 0	,	 0	,
							 1	,	 1	,	 1	};
	/*　メディアンを行うために画素値を入れる行列　*/
	int kr[9] =	{	 0	,	 0	,	 0	,
					 0	,	 0	,	 0	,
					 0	,	 0	,	 0	};

	int kg[9] =	{	 0	,	 0	,	 0	,
					 0	,	 0	,	 0	,
					 0	,	 0	,	 0	};

	int kb[9] =	{	 0	,	 0	,	 0	,
					 0	,	 0	,	 0	,
					 0	,	 0	,	 0	};

	// 1ピクセルずつ、すべてのピクセルを処理
	// 空間フィルタは周囲のピクセルを参照するため、画面の端は処理しない
	for(int y = 1; y < hgt-1; y++)
	{
		for(int x = 1; x < wid-1; x++)
		{
			// 合計値を入れる変数（最初は0に初期化)
			int RSum = pxSrc[y][x].R;
			int GSum = pxSrc[y][x].G;
			int BSum = pxSrc[y][x].B;

		// 周辺9画素を計算に使う
			for(int i = 0; i < 9; i++)
			{
			// 元画像pxSrcの(x+dx[i], y+dy[i])座標の色値を得て、フィルタ係数をかける
				kr[i] = pxSrc[y+dy[i]][x+dx[i]].R;
				kg[i] = pxSrc[y+dy[i]][x+dx[i]].G;
				kb[i] = pxSrc[y+dy[i]][x+dx[i]].B;
			}

			int RDst = retrieve5thGreatest(RSum, kr);
			int GDst = retrieve5thGreatest(GSum, kg);
			int BDst = retrieve5thGreatest(BSum, kb);

			// 結果画像pxDstの(x,y)の色値を更新
			pxDst[y][x].R = RDst;
			pxDst[y][x].G = GDst;
			pxDst[y][x].B = BDst;
		}
	}
}

//　ラプラシアンで抽出した境界線すべてを黒で出力、それ以外を白で出力する
void imFilterLineDrawing( ms2dImage *imgSrc , ms2dImage *imgDst ){

//　どこまでの色を境界線とみなすかを判別する引数
	const double V = 0.2;

	ms2dImage *imgMed = ms2dImageCreate(1, 1);
	ms2dImage *imgMedLap = ms2dImageCreate(1, 1);
	ms2dImage *imgLap = ms2dImageCreate(1, 1);

	ms2dImageCopy(imgSrc, imgDst);

	imFilterMed( imgSrc , imgMed );

//　ラプラシアンフィルタ
	imFilterLap( imgSrc , imgLap );
	imFilterMedLap(imgSrc, imgMedLap);

//　画像サイズの取得
	const int wid = imgDst -> width;						//　幅
	const int hgt = imgDst -> height;						//　高さ
	
	ms2dColorMatrix pxDst = imgDst -> pixels;			//　結果画像のピクセルデータ
	ms2dColorMatrix pxSrc = imgSrc -> pixels;
	ms2dColorMatrix pxLap = imgLap -> pixels;
	ms2dColorMatrix pxMedLap = imgMedLap -> pixels;

//　1ピクセルずつ、すべてのピクセルを処理
//　画像の端は処理しないこととする
	for( int y = 1 ; y < hgt - 1 ; y++ ){
		for( int x = 1 ; x < wid - 1 ; x ++ ){

			const double VLap = imCalcV(pxLap[y][x].R, pxLap[y][x].G, pxLap[y][x].B);
			const double VMedLap = imCalcV(pxMedLap[y][x].R, pxMedLap[y][x].G, pxMedLap[y][x].B);
			const double VLapMultiply = VLap * VMedLap * 10.0;

			int linePixels = 0;
			if(VLapMultiply >= V) linePixels = 0;
			else linePixels = 255;

			pxDst[y][x].R = linePixels;
			pxDst[y][x].G = linePixels;
			pxDst[y][x].B = linePixels;
		}
	}
}

//　HSVで明るさを割出し10段階に分けてグレースケールで出力する
void imFileter10Scale( ms2dImage *imgSrc , ms2dImage *imgDst ){

//　Dstにコピー
	ms2dImageCopy( imgSrc , imgDst );

//　画像サイズの取得
	const int wid = imgDst -> width;						//　幅
	const int hgt = imgDst -> height;						//　高さ

	ms2dColorMatrix pxSrc = imgSrc -> pixels;			//　元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst -> pixels;			//　結果画像のピクセルデータ

//　1ピクセルずつ処理
	for( int y = 1 ; y < hgt - 1 ; y++ ){
		for( int x = 1 ; x < wid - 1 ; x++ ){

		//　元画像のRGB値を変数に代入
			int RSum = pxSrc[y][x].R;
			int GSum = pxSrc[y][x].G;
			int BSum = pxSrc[y][x].B;

		//　Vを求める
			double V = imCalcV( RSum , GSum , BSum );
			int Dst = 0.0;

		//ループ処理で明度の範囲で同じ色にする
			for( int i = 0 ; i < 10 ; i++ ){
			//　0.1ごとに条件を決めて色を定める
				if( V >= ( 0.1 * i ) && V < ( 0.1 * ( i + 1 ) ) ){
				//　半分の値をDstに加える
					Dst = (int)((0.1 * ( i + 0.5 )) * 255);
				}else if( V == 1.0 ){
					Dst = 255;
				}
			}

			pxDst[y][x].R = Dst;
			pxDst[y][x].G = Dst;
			pxDst[y][x].B = Dst;

		}

	}

}

//　HSVで明るさを割出し5段階に分けてグレースケールで出力する
void imFileter5ScaleComic( ms2dImage *imgSrc , ms2dImage *imgDst ){

//　Dstにコピー
	//ms2dImageCopy( imgSrc , imgDst );
	ms2dImage *imgMed = ms2dImageCreate(1, 1);
	ms2dImage *imgGray = ms2dImageCreate(1, 1);

	imFilterLineDrawing(imgSrc, imgDst);

	imFilterMed(imgSrc, imgMed);
	imFilterGrayScale(imgMed, imgGray);

//　画像サイズの取得
	const int wid = imgDst -> width;						//　幅
	const int hgt = imgDst -> height;						//　高さ

	ms2dColorMatrix pxSrc = imgSrc -> pixels;			//　元画像のピクセルデータ
	ms2dColorMatrix pxMed = imgMed -> pixels;
	ms2dColorMatrix pxDst = imgDst -> pixels;			//　結果画像のピクセルデータ
	ms2dColorMatrix pxGray = imgGray->pixels;

//　1ピクセルずつ処理
	for( int y = 1 ; y < hgt - 1 ; y++ ){
		for( int x = 1 ; x < wid - 1 ; x++ ){

		//　元画像のRGB値を変数に代入
			int RSum = pxMed[y][x].R;
			int GSum = pxMed[y][x].G;
			int BSum = pxMed[y][x].B;

			int RLap = pxDst[y][x].R;
			int GLap = pxDst[y][x].G;
			int BLap = pxDst[y][x].B;

		//　Vを求める
			const double V = (double)pxGray[y][x].R / 255.0;
			int Dst = 0.0;

			if( RLap == 255 ){

			//ループ処理で明度の範囲で同じ色にする
				for( int i = 0 ; i < 5 ; i++ ){
				//　0.2ごとに条件を決めて色を定める
					if(V >= (0.2 * i) &&
						V < (0.2 * (i + 1))){
					//　半分の値をDstに加える
	//					Dst = (int)((0.2 * ( i + 1.0 )) * 255);
						if( ((x/1) - y) % (i+1) == 0 ){
							if( i == 4 ) Dst = 255;
							else Dst = 0;
						}else{
							Dst = 255;
						}
					}else if( V == 1.0 ){
						Dst = 255;
					}
				}
			}
			pxDst[y][x].R = Dst;
			pxDst[y][x].G = Dst;
			pxDst[y][x].B = Dst;
		}
	}
}

//　HSVで明るさを割出し10段階に分けてグレースケールで出力する
void imFileterBrightMed( ms2dImage *imgSrc , ms2dImage *imgDst ){

	ms2dImage *imgSca;

	imgSca = ms2dImageCreate( 1 , 1 );

	imFilterMed( imgSrc , imgSca );

//　Dstにコピー
	ms2dImageCopy( imgSca , imgDst );

//　画像サイズの取得
	const int wid = imgDst -> width;						//　幅
	const int hgt = imgDst -> height;						//　高さ

	ms2dColorMatrix pxSrc = imgSca -> pixels;			//　元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst -> pixels;			//　結果画像のピクセルデータ

//　1ピクセルずつ処理
	for( int y = 1 ; y < hgt - 1 ; y++ ){
		for( int x = 1 ; x < wid - 1 ; x++ ){

		//　元画像のRGB値を変数に代入
			const int RSum = pxSrc[y][x].R;
			const int GSum = pxSrc[y][x].G;
			const int BSum = pxSrc[y][x].B;

		//　Vを求める
			double V = imCalcV( RSum , GSum , BSum );
			double H = imCalcH( RSum , GSum , BSum );
			double S = imCalcS( RSum , GSum , BSum );

			int RDst = 0;
			int GDst = 0;
			int BDst = 0;

			V += 0.2;
			if( V >= 1.0 ) V = 1.0 ;

		//ループ処理で明度の範囲で同じ色にする
			for( int i = 0 ; i < 18 ; i++ ){
				if( H >= (double)(i * 20) && H < (double)((i + 1) * 20) ){

					RDst = imCalcR( H , S , V );
					GDst = imCalcG( H , S , V );
					BDst = imCalcB( H , S , V );

				}
			}

			pxDst[y][x].R = RDst;
			pxDst[y][x].G = GDst;
			pxDst[y][x].B = BDst;

		}

	}

}

//　HSVで明るさを割出し5段階に分けてグレースケールで出力する
void imFileter5ScaleColor( ms2dImage *imgSrc , ms2dImage *imgDst ){

//　Dstにコピー
	ms2dImageCopy( imgSrc , imgDst );

//　画像サイズの取得
	const int wid = imgDst -> width;						//　幅
	const int hgt = imgDst -> height;						//　高さ

	ms2dColorMatrix pxSrc = imgSrc -> pixels;			//　元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst -> pixels;			//　結果画像のピクセルデータ

//　1ピクセルずつ処理
	for( int y = 1 ; y < hgt - 1 ; y++ ){
		for( int x = 1 ; x < wid - 1 ; x++ ){

		//　元画像のRGB値を変数に代入
			const int RSum = pxSrc[y][x].R;
			const int GSum = pxSrc[y][x].G;
			const int BSum = pxSrc[y][x].B;

		//　Vを求める
			const double Vr = RSum / 255.0;
			const double Vg = GSum / 255.0;
			const double Vb = BSum / 255.0;

			int RDst = 0;
			int GDst = 0;
			int BDst = 0;

		//ループ処理で明度の範囲で同じ色にする
			for( int i = 0 ; i < 2 ; i++ ){
			//　0.1ごとに条件を決めて色を定める
				RDst = pixelsValueGrouping(RDst, Vr, i);
				GDst = pixelsValueGrouping(GDst, Vg, i);
				BDst = pixelsValueGrouping(BDst, Vb, i);
			}

			pxDst[y][x].R = RDst;
			pxDst[y][x].G = GDst;
			pxDst[y][x].B = BDst;
		}
	}
}

void imFilterWhiteScale(ms2dImage *imgSrc, ms2dImage *imgDst){
	ms2dImageCopy(imgSrc, imgDst);

	const int wid = imgSrc->width;
	const int hgt = imgSrc->height;

	ms2dColorMatrix pxSrc = imgSrc->pixels;
	ms2dColorMatrix pxDst = imgDst->pixels;

	for(int y = 0; y < hgt-1; y++){
		for(int x = 0; x < wid-1; x++){
			int RSrc = pxSrc[y][x].R;
			int GSrc = pxSrc[y][x].G;
			int BSrc = pxSrc[y][x].B;

			int VSrc = (int)(imCalcV(RSrc, GSrc, BSrc)*255);

			pxDst[y][x].R = VSrc;
			pxDst[y][x].G = VSrc;
			pxDst[y][x].B = VSrc;

		}
	}
}

void imFilterGrayScale(ms2dImage *imgSrc, ms2dImage *imgDst){
	//Dstにコピー
	ms2dImageCopy( imgSrc , imgDst );

	//画像サイズの取得
	const int wid = imgDst -> width;					//幅
	const int hgt = imgDst -> height;					//高さ

	//ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc -> pixels;	//元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst -> pixels;	//結果画像のピクセルデータ

	//1ピクセルずつ、すべてのピクセルを処理
	for( int y = 0; y < hgt ; y++){

		for( int x = 0 ; x < wid ; x++ ){

			//元画像pxSrcの(x,y)座標の色値を得る
			int Rsrc = pxSrc[y][x].R;
			int Gsrc = pxSrc[y][x].G;
			int Bsrc = pxSrc[y][x].B;

			//光度信号Yによるモノクロ化
			int Ysig = (int)(0.299*Rsrc + 0.587*Gsrc + 0.114*Bsrc);

			//結果画像pxDstの(x,y)の色値Rはそのまま、G,Bを0にする
			pxDst[y][x].R = Ysig;
			pxDst[y][x].G = Ysig;
			pxDst[y][x].B = Ysig;
		}
	}
}

//　V(明度)を計算する
double imCalcV( int R , int G , int B ){
//　明度を入れる引数
	double V = 0.0 ;
//　RGBの内の最大値を求める
	double maxv = maxColorValue(R, G, B);
//　Vの値を求め、正規化する
	V = maxv / 255.0;
//　Vの値を返す
	return V;
}

//　S(彩度)を計算する
double imCalcS( int R , int G , int B ){
//　彩度を入れる引数
	double S = 0.0;
//　RGBの最大値と最小値を求める
	double maxv = maxColorValue(R, G, B);
	double minv = minColorValue(R, G, B);
//　Sの値を求める
	if( maxv == 0.0 ) S = 0.0;
	else S = ( maxv - minv ) / maxv ;
//　Sの値を返す
	return S ;
}

//　H(色相)を計算する
double imCalcH( int R , int G , int B ){
//　RGBの最大値と最小値を求める
	double maxv = maxColorValue(R, G, B);
	double minv = minColorValue(R, G, B);
//値を入れる引数
	double H = 0.0;
//Sを取得
	double S = imCalcS( R , G , B );
	if( S == 0.0 ){
	//彩度が0のとき、色相は0になるように設定する
		H = 0.0;
	}else{
		if( R == maxv ){
			H = 60.0 * (( G - B ) / ( maxv - minv ));
		}else if( G == maxv ){
			H = 120.0 + 60.0 * (( B - R ) / ( maxv - minv ));
		}else if( B == maxv ){
			H = 240.0 + 60.0 * (( R - G ) / ( maxv - minv ));
		}
		if( H < 0.0 ) H = H + 360.0;
	}
	return H;
}

// HSVからRを求める
int imCalcR(double H, double S, double V)
{
	double R = 0.0;
	
	// S,Vが0.0~1.0に収まるようにする
	S = min(1.0, max(0.0, S));
	V = min(1.0, max(0.0, V)); 

	// Hが0.0~360.0に収まるようにする
	while(H < 0.0 || 360.0 <= H){
		if(H < 0.0) { H += 360.0; }
		if(H >= 360.0) { H-= 360.0; }
	}
		
	// 以下に処理を追加
	if( S == 0.0 ){
		R = V ;
	}else{

		//　１．H = 0 - 60 ： Rが最大値
		if( H < 60.0 ) R = V;
		//　２．H = 60 - 120 ：　Rが中間値
		else if( H < 120.0 ) R = V * ( 1 - ( S * ( ( ( H - 120 ) / 60 ) + 1 ) ) );
		//　３．H = 120 - 180　：　Rが最小値
		else if( H < 180.0 ) R = V * ( 1 - S );
		//　４．H = 180 - 240　：　Rが最小値
		else if( H < 240.0 ) R = V * ( 1 - S );
		//　５．H = 240 - 300　：　Rが中間値
		else if( H < 300.0 ) R = V * ( 1 + ( S * ( ( ( H - 240 ) / 60 ) - 1 ) ) ) ;
		//　６．H = 300 - 360　：　Rが最大値
		else if( H < 360.0 ) R = V;

	}
	R = R * 255.0;
	return (int)R;
}

// HSVからGを求める
int imCalcG(double H, double S, double V)
{
	double G = 0.0;
	
	// S,Vが0.0~1.0に収まるようにする
	S = min(1.0, max(0.0, S));
	V = min(1.0, max(0.0, V)); 

	// Hが0.0~360.0に収まるようにする
	while(H < 0.0 || 360.0 <= H){
		if(H < 0.0) { H += 360.0; }
		if(H >= 360.0) { H-= 360.0; }
	}
		
	// 以下に処理を追加
	if( S == 0.0 ){
		G = V ;
	}else{

		//　１．H = 0 - 60 ： Gが中央値
		if( H < 60.0 ) G = V * ( 1 + ( S * ( ( H / 60 ) - 1 ) ) );
		//　２．H = 60 - 120 ：　Gが最大値
		else if( H < 120.0 ) G = V;
		//　３．H = 120 - 180　：　Gが最大値
		else if( H < 180.0 ) G = V;
		//　４．H = 180 - 240　：　Gが中央値
		else if( H < 240.0 ) G = V * ( 1 - ( S * ( ( ( H - 240 ) / 60 ) + 1 ) ) );
		//　５．H = 240 - 300　：　Gが最小値
		else if( H < 300.0 ) G = V * ( 1 - S );
		//　６．H = 300 - 360　：　Gが最小値
		else if( H < 360.0 ) G = V * ( 1 - S );

	}
	G = G * 255.0;
	return (int)G;
}

// HSVからBを求める
int imCalcB(double H, double S, double V)
{
	double B = 0.0;
	
	// S,Vが0.0~1.0に収まるようにする
	S = min(1.0, max(0.0, S));
	V = min(1.0, max(0.0, V)); 

	// Hが0.0~360.0に収まるようにする
	while(H < 0.0 || 360.0 <= H){
		if(H < 0.0) { H += 360.0; }
		if(H >= 360.0) { H-= 360.0; }
	}
		
	// 以下に処理を追加
	if( S == 0.0 ){
		B = V;
	}else{
		//　１．H = 0 - 60 ： Bが最小値
		if( H < 60.0 ) B = V * ( 1 - S );
		//　２．H = 60 - 120 ：　Bが最小値
		else if( H < 120.0 ) B = V * ( 1 - S );
		//　３．H = 120 - 180　：　Bが中央値
		else if( H < 180.0 ) B = V * ( 1 + ( S * ( ( ( H - 120 ) / 60 ) - 1 ) ) ) ;
		//　４．H = 180 - 240　：　Bが最大値
		else if( H < 240.0 ) B = V ;
		//　５．H = 240 - 300　：　Bが最大値
		else if( H < 300.0 ) B = V;
		//　６．H = 300 - 360　：　Bが中央値
		else if( H < 360.0 ) B = V * ( 1 - ( S *  ( ( ( H - 360 ) / 60 ) + 1 ) ) ) ;
	}
	B = B * 255.0;
	return (int)B;
}
