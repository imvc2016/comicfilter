//ヘッダー
#include<stdio.h>		//　標準出力
#include<stdlib.h>	//　標準ライブラリ
#include<math.h>		//　数学ヘッダ
#include<GL/glut.h>	//　glut
#include<msgl.h>		//　msgl

//以下処理用の関数

//　メディアンとラプラシアンを同時に行う関数
void imFilterMedLap( ms2dImage *imgSrc , ms2dImage *imgDst );

//　ラプラシアンフィルタ
void imFilterLap( ms2dImage *imgSrc , ms2dImage *imgDst );

//　ラプラシアン	フィルタ
void imFilterNorLap( ms2dImage *imgSrc , ms2dImage *imgDst );

//　２極化された画像のノイズを除去する
void imFilterMed(ms2dImage *imgSrc, ms2dImage *imgDst);

//　ラプラシアンフィルタで変換された画像を２極化して線画のようにする
void imFilterLineDrawing( ms2dImage *imgSrc , ms2dImage *imgDst );

//　メディアンかけた後にラプラシアンをする処理
void MedLap( ms2dImage *imgSrc , ms2dImage *imgDst );

//　HSVで明るさを割出し10段階に分けてグレースケールで出力する
void imFileter10Scale( ms2dImage *imgSrc , ms2dImage *imgDst );

//　HSVで明るさを割出し5段階に分けてグレースケールで出力する
void imFileter5ScaleComic( ms2dImage *imgSrc , ms2dImage *imgDst );

//　HSVで明るさを割出し10段階に分けてグレースケールで出力する
void imFileterBrightMed( ms2dImage *imgSrc , ms2dImage *imgDst );

//　HSVで明るさを割出し10段階に分けてグレースケールで出力する
void imFileter5ScaleColor( ms2dImage *imgSrc , ms2dImage *imgDst );

//　画像の白さを計算してグレースケールで出力する
void imFilterWhiteScale(ms2dImage *imgSrc, ms2dImage *imgDst);

void imFilterGrayScale(ms2dImage *imgSrc, ms2dImage *imgDst);

//　V(明度)の計算をする
double imCalcV( int R , int G , int B );

//　S(彩度)を計算する
double imCalcS( int R , int G , int B );

//　H(色相)を計算する
double imCalcH( int R , int G , int B );

//　Rを計算する
int imCalcR( double H , double S , double V );

//　Gを計算する
int imCalcG( double H , double S , double V );

//　Bを計算する
int imCalcB( double H , double S , double V );
